package AppiumWebtoon;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class SignupTest {

    static AppiumDriver<MobileElement> driver;

    public static void main(String[] args) {
        try {
            openWebtoon();
        } catch (MalformedURLException exp) {
            System.out.println(exp.getCause());
            System.out.println(exp.getMessage());
            exp.printStackTrace();
        }
    }

    public static void openWebtoon() throws MalformedURLException {
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("deviceName", "Pixel 3a XL API 28");
        cap.setCapability("udid", "emulator-5554");
        cap.setCapability("platformName", "Android");
        cap.setCapability("platformVersion", "9");
        cap.setCapability("appPackage", "com.naver.linewebtoon");
        cap.setCapability("appActivity", "com.naver.linewebtoon.main.MainActivity");

        URL url = new URL("http://127.0.0.1:4723/wd/hub");
        driver = new AppiumDriver<MobileElement>(url, cap);

        System.out.println("Application Started...");

        MobileElement signupfisrst = driver.findElement(By.id("com.naver.linewebtoon:id/btn_sign_up"));
//        MobileElement idfield = driver.findElement(By.id("com.naver.linewebtoon:id/input_id"));
//        MobileElement passwordfield = driver.findElement(By.id("com.naver.linewebtoon:id/input_password"));
//        MobileElement repasswordfield = driver.findElement(By.id("com.naver.linewebtoon:id/input_repassword"));
//        MobileElement nicknamefield = driver.findElement(By.id("com.naver.linewebtoon:id/input_nickname"));


        signupfisrst.click();
//        idfield.click();
//        driver.findElementById("com.naver.linewebtoon:id/input_id").sendKeys("gdsbhchh@gmail.com");
    }
}
