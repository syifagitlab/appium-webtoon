package pageobjects.webtoon;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import utilities.AppiumHelpers;

public class LoginPO {
    AppiumHelpers appium;
    AppiumDriver<WebElement> driver;

    public LoginPO(AppiumDriver<WebElement> driver) {
        this.driver = driver;
        appium = new AppiumHelpers(driver);

//        PageFactory ini untuk kayak setiap kelas yang dijalankan dia akan jalankan method ini dulu gitu
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }
    @AndroidFindBy(id="skip")
    private WebElement skipButton;

    @AndroidFindBy(id = "input_id")
    private MobileElement idEditText ;

    @AndroidFindBy(id = "input_password")
    private MobileElement passwordEditText;

    @AndroidFindBy(id="btn_log_in")
    private MobileElement loginButton;

    @AndroidFindBy(id="btn_log_in")
    private MobileElement onboardingButton;

    /**
     * Click on Skip
     */
    public void clickOnSkipButton() { skipButton.click();
    }

    /**
     * Click on login
     */
    public void clickOnLoginButton() { loginButton.click();
    }

    /**
     * Enter username or email
     * @param usernameOrEmail
     */
    public void enterId(String usernameOrEmail) {
        appium.enterText(idEditText, usernameOrEmail, true);}

    /**
     * Enter password
     * @param password
     */
    public void enterPassword(String password) {
        appium.enterText(passwordEditText, password, true);}

    /**
     * Verify element is displayed
     */

    public void startButtonDisplayed() {
        appium.isElementDisplayed(onboardingButton);
    }
}
