package runners;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/calculator/cucumber-report.json",  "html:target/results/calculator"},
        features = "src/test/resources/features/webtoon",
        glue = "steps",
        tags = {"@webtoon"}

)

public class WebtoonTestRunner extends BaseTestRunnerAndroid{

}
