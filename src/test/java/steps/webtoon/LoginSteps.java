package steps.webtoon;

import io.appium.java_client.AppiumDriver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebElement;
import pageobjects.webtoon.LoginPO;
import utilities.ThreadManager;

public class LoginSteps {
    private AppiumDriver<WebElement> driver = ThreadManager.getDriver();
    private LoginPO login = new LoginPO(driver);

    @Given("user click on skip")
    public void user_click_on_skip() {
        login.clickOnSkipButton();
    }

    @Given("user click on login button")
    public void user_click_on_login_button() {
        login.clickOnLoginButton();
    }

    @Given("user input id {string}")
    public void user_input_id(String usernameOrEmail) {
        login.enterId(usernameOrEmail);
    }

    @When("user input password {string}")
    public void user_input_password(String password) {
        login.enterPassword(password);
    }

    @Then("system display start button")
    public void system_display_start_button() {
        login.startButtonDisplayed();
    }

}
